python-django-analytical (3.1.0-2) unstable; urgency=medium

  * Team upload.
  * Port to Sphinx 8.0 (closes: #1090138).

 -- Colin Watson <cjwatson@debian.org>  Wed, 18 Dec 2024 13:18:58 +0000

python-django-analytical (3.1.0-1) unstable; urgency=medium

  * Team upload
  * New upstream version 3.1.0
  * autopkgtest: Add python3-all to dependencies
  * d/copyright: Update copyright year data

 -- Carsten Schoenert <c.schoenert@t-online.de>  Fri, 18 Mar 2022 20:05:59 +0100

python-django-analytical (3.0.0-3) unstable; urgency=medium

  * Team upload
  * d/control: Mark the -doc package Multi-Arch: foreign
  * d/rules: Ignore one specific unit test
  * autopkgtest: Also drop this one specific unit test

 -- Carsten Schoenert <c.schoenert@t-online.de>  Sat, 25 Sep 2021 22:55:12 +0200

python-django-analytical (3.0.0-2) unstable; urgency=medium

  [ Diego M. Rodriguez ]
  * d/watch: update to GitHub format.
  * d/test: specify import_name. (Closes: #982121)

  [ Michael Fladischer ]
  * Bump Standards-Version to 4.6.0.
  * Remove unnecessary autopkgtest-pkg-python testsuite.

 -- Michael Fladischer <fladi@debian.org>  Wed, 01 Sep 2021 21:13:04 +0000

python-django-analytical (3.0.0-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 4.5.1.
  * Add python3-pytest and python3-pytest-django to Build-Depends,
    required by tests.
  * Remove custom build-time test, use default pybuilder pytest
    integration.
  * Use uscan version 4.
  * Enable upstream testsuite for autopkgtests.

 -- Michael Fladischer <fladi@debian.org>  Fri, 18 Dec 2020 14:09:58 +0100

python-django-analytical (2.6.0-1) unstable; urgency=low

  * Initial release (Closes: #972210).

 -- Michael Fladischer <fladi@debian.org>  Wed, 14 Oct 2020 10:13:14 +0200
